/*
 *    hash-crack
 *    Copyright (C) 2018  Patrick Louis
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _H_HASH_CRACK
#define _H_HASH_CRACK 1

#include <stdio.h>
#include <stdlib.h>
#ifdef __GNUC__
  #include <unistd.h>
#else
  #include "wingetopt.h"
#endif
#include <ctype.h>
#include <malloc.h>
#include "cracker.h"

#define APPLICATION_NAME "hash-crack"

#define VERSION_MAJOR 0
#define VERSION_MINOR 2
/* ----------------------------------------
 * Error Messages and Codes
 * ----------------------------------------*/

#define ERR_N_ARGUMENT_ERROR 12
#define ERR_S_ARGUMENT_ERROR "Unknown Argument"

#endif
