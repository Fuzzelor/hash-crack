/*
 *    hash-bruteForce
 *    Copyright (C) 2018  Patrick Louis
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _H_CRACKER
#define _H_CRACKER 1

#include <stdlib.h>
//#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <thread>
#include <mutex>
#include <math.h>
#include "digest.h"

#define ALGO_SHA256 "sha256"
#define LEN_SHA256 64
#define ALGO_MD5 "md5"
#define LEN_MD5 32

#define MAXLEN 8

#define SETSIZE_ALPHA_LC    26
#define SETSIZE_ALPHA_UC    26
#define SETSIZE_NUMERIC     10
#define SETSIZE_SPECIAL     27

#define MASK_LC_ALPHA  (char)0x01
#define MASK_UC_ALPHA  (char)0x02
#define MASK_NUM       (char)0x04
#define MASK_SPEC      (char)0x08

class Cracker {
  public:
    Cracker();

    int setAlgorithm(std::string algo);
    void setCharSet(unsigned char bitmap);
    void setThreadCount(unsigned char tc);
    std::string Crack(std::string);
    std::string getAlgorithm();

  private:
    const char csAlphaLc[27] = "abcdefghijklmnopqrstuvwxyz";
    const char csAlphaUc[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const char csNumeric[11] = "0123456789";
    const char csSpecial[28] = "!£$%^&*()[]{}#~@;:?/><,.|";

    int searchSpace;
    unsigned int targetLength;
    unsigned char threadCount;
    char* charSet;
    std::string algorithm;
    std::string (*algorithmPtr)(std::string);
};

void bruteForce(std::string target, 
	long long searchSpaceStart, 
	long long searchSpaceEnd, 
	long long searchSpaceSize, 
	char* charSet, 
	std::string (*algorithmPtr)(std::string));
std::string convert(long long num, int base, char* charSet);

#endif
