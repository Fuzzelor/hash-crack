/*
 *    hash-crack
 *    Copyright (C) 2018  Patrick Louis
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "cracker.h"



std::mutex resultMtx;
std::string result;
/*! \brief Constructor
 *
 *  Constructor for Cracker class
 *
 * \return Cracker instance
 */
Cracker::Cracker()
{
  this->setAlgorithm("sha256");
  this->setCharSet(15);
  this->setThreadCount(2);
}

/* \brief Setter Method for the Cracker algorithm
 *
 * Sets the algorithm and function pointer for the appropriate digest algorithm
 *
 * \return 0 on success
 */
int Cracker::setAlgorithm( std::string algo ) {
  if(algo == ALGO_SHA256) {
    this->algorithm = algo;

    this->algorithmPtr=&Digest::Sha_256;
    this->targetLength = LEN_SHA256;

    return 0;
  } 
  else if(algo == ALGO_MD5) {
    this->algorithm = algo;

    this->algorithmPtr=&Digest::Md5;
    this->targetLength = LEN_MD5;

    return 0;
  }
  return -1;
}

/*! \brief Sets the threadcount to use
 *
 *  Setter method for the threadCount variable
 *
 * \param tc Threadcount to set
 * \return nothing
 */
void Cracker::setThreadCount(unsigned char tc)
{
  if(tc > 0 && tc < 65) {
    this->threadCount = tc;
  }
}

/*! \brief Getter for algorithm
 *
 *  Gets the content of the algorithm variable
 *
 * \return Content of the algorithm variable
 */
std::string Cracker::getAlgorithm()
{
  return this->algorithm;
}

/*! \brief Sets the Characterset
 *
 *  Setter method for the characterset
 *  parameter is a bitmap for the charactersets
 *  1 = lowercase alpha
 *  2 = uppercase alpha
 *  4 = numeric
 *  8 = special characters
 *
 * \param bitmap bitmap of the charactersets to use
 * \return nothing
 */
void Cracker::setCharSet(unsigned char bitmap)
{
  int totalsize  = 0;
  int shiftWidth = 0;

  totalsize = ((bitmap & MASK_LC_ALPHA) * SETSIZE_ALPHA_LC) +
              (((bitmap & MASK_UC_ALPHA) >> 1) * SETSIZE_ALPHA_UC) +
              (((bitmap & MASK_NUM) >> 2) * SETSIZE_NUMERIC) +
              (((bitmap & MASK_SPEC) >> 3) * SETSIZE_SPECIAL) ;

  this->charSet = (char*)malloc(sizeof(char) * totalsize);
  this->searchSpace = totalsize;

  if (bitmap & MASK_LC_ALPHA) {
    strcpy(this->charSet, Cracker::csAlphaLc);
    shiftWidth += SETSIZE_ALPHA_LC;
  }
  if (bitmap & MASK_UC_ALPHA) {
    strcpy((this->charSet + shiftWidth), Cracker::csAlphaUc);
    shiftWidth += SETSIZE_ALPHA_UC;
  }
  if (bitmap & MASK_NUM) {
    strcpy((this->charSet + shiftWidth), Cracker::csNumeric);
    shiftWidth += SETSIZE_NUMERIC;
  }
  if (bitmap & MASK_SPEC) {
    strcpy((this->charSet + shiftWidth), Cracker::csSpecial);
    shiftWidth += SETSIZE_SPECIAL;
  }
}

/*! \brief BruteForce target
 *
 *  Tries to bruteforce the string that would digest to target
 *
 * \param target The target we want to generate as digest
 * \return The string that digests to target
 */
std::string Cracker::Crack(std::string target)
{
  // Check if Target fits algorithm
  if(DEBUG_MODE) { printf("Entered Cracker::Crack\n");}
  if(DEBUG_MODE) { printf("Cracker::bruteForce: Charset is %s\n", this->charSet); }
  if(DEBUG_MODE) { printf("Cracker::bruteForce: Searchspace is %i\n", this->searchSpace); }
  if(DEBUG_MODE) { printf("Cracker::bruteForce: Total Searchspace is %lli\n", (long long)pow((double)this->searchSpace,MAXLEN)); }

  if ( target.length() != this->targetLength ) {
    fprintf(stderr, "Error %i, Target value is not a %s digest\n", 30, this->algorithm);
    return "";
  }

  long long searchSpaceSize = (long long)pow((double)this->searchSpace,MAXLEN);
  long long searchSpacePerThread = (long long)(searchSpaceSize/this->threadCount);

  if (DEBUG_MODE) { printf("Cracker::Crack creating Threads\n");}
  if (this->threadCount >= 2) {
	  std::thread *t = new std::thread[this->threadCount];

	  if (DEBUG_MODE) { printf("Cracker::Crack created Threads-array\n"); }
	  for (int i = 0; i < this->threadCount; i++) {
		  if (DEBUG_MODE) { printf("Cracker::bruteForce: Creating Thread %i\n", i); }
		  t[i] = std::thread(bruteForce,
			  target,
			  (i*searchSpacePerThread),
			  ((i + 1)*searchSpacePerThread),
			  this->searchSpace,
			  this->charSet,
			  this->algorithmPtr);
	  }

	  if (DEBUG_MODE) { printf("Cracker::bruteForce: Created Threads\n"); }
	  while (result == "") {
		  for (int i = 0; i < this->threadCount; i++) {
			  if (t[i].joinable() && result != "") {
				  t[i].join();
				  if (DEBUG_MODE) { printf("Cracker::Crack: joined Thread %i \n", i); }
				  for (int j = 0; j < this->threadCount; j++) {
					  if (j != i) {
						  t[j].detach();
						  if (DEBUG_MODE) { printf("Cracker::Crack: detached Thread %i \n", j); }
					  }
				  }
				  break;
			  }
		  }
	  }
  }
  else {
	  bruteForce(target, 0, searchSpacePerThread, this->searchSpace, this->charSet, this->algorithmPtr);
  }

  return result;
}

void bruteForce(std::string target,
            long long searchSpaceStart,
            long long searchSpaceEnd,
            long long searchSpaceSize,
            char* charSet,
            std::string (*algorithmPtr)(std::string))
{
  std::string hash = "";
  std::string candidate = "";

  // permutate candidate
  for(long long i = searchSpaceStart; i < searchSpaceEnd ; i++) {
    if ( result != "" ) { break; } // end all Threads when result was found
    candidate = convert(i, searchSpaceSize, charSet);
    hash = algorithmPtr(candidate);
    //printf("Testing Value %lli : %s gives %s\n", i, candidate.data(), hash);
    if(target == hash) {
      printf("Found correct value: %s\n", candidate.data());
      resultMtx.lock();  // Lock the mutex before accessing result
      result = candidate;
      resultMtx.unlock();
      break;
    }
  }
}

/*! \brief Converts a number to any base
 *
 *  Converts number to any base
 *
 * \param num The number to convert
 * \param base the base to use
 * \param charSet The characterSet to convert to
 * \return The number converted to a string
 */
std::string convert(long long num, int base, char* charSet)
{
  if(num == 0) return "";
  int outStrLen = (int)(log(num) / log(base)) ;
  std::string result = "";

  long long i = 0;
  do {
    result.push_back(charSet[num % base]);

    num = (long long)(num / base);
    i++;
  } while( num != 0 );

  return result;
}
