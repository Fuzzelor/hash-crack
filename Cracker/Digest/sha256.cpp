/*
 *    hash-crack
 *    Copyright (C) 2018  Patrick Louis
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "digest.h"

//Initialize array of round constants:
//(first 32 bits of the fractional parts of the cube roots of the first 64 primes 2..311):
unsigned int k[64] = {
     0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
     0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
     0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
     0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
     0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
     0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
     0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
     0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
  };


std::string Digest::Sha_256(std::string input) {
  int strLen = input.length();
  Digest::SHA256_CTX ctx;
  unsigned char hash[32];
  std::string hashStr = "";

  Digest::SHA256Init(&ctx);
  Digest::SHA256Update(&ctx, (unsigned char*)input.data(), strLen);
  Digest::SHA256Final(&ctx, (unsigned char*)hash);

  char s[3];
  for (int i = 0; i < 32; i++) {
    sprintf(s, "%02x", hash[i]);
    hashStr += s;
  }

  return hashStr;
}


void Digest::SHA256Transform(SHA256_CTX *ctx, uchar data[])
{
  uint a, b, c, d, e, f, g, h, i, j, t1, t2, m[64];

  // The initial values in m[0..63] don't matter
  for (i = 0, j = 0; i < 16; ++i, j += 4)
    m[i] = (data[j] << 24) | (data[j + 1] << 16) | (data[j + 2] << 8) | (data[j + 3]);
  // Extend the first 16 words into the remaining 48 words m[16..63] of the message schedule array:
  for (; i < 64; ++i)
    m[i] = SIG1(m[i - 2]) + m[i - 7] + SIG0(m[i - 15]) + m[i - 16];

  // Initialize working variables to current hash-value
  a = ctx->state[0];
  b = ctx->state[1];
  c = ctx->state[2];
  d = ctx->state[3];
  e = ctx->state[4];
  f = ctx->state[5];
  g = ctx->state[6];
  h = ctx->state[7];

  // Compression function main loop
  for (i = 0; i < 64; ++i) {
    t1 = h + EP1(e) + CH(e, f, g) + k[i] + m[i];
    t2 = EP0(a) + MAJ(a, b, c);
    h = g;
    g = f;
    f = e;
    e = d + t1;
    d = c;
    c = b;
    b = a;
    a = t1 + t2;
  }

  // Add the compressed chunk to current hash value
  ctx->state[0] += a;
  ctx->state[1] += b;
  ctx->state[2] += c;
  ctx->state[3] += d;
  ctx->state[4] += e;
  ctx->state[5] += f;
  ctx->state[6] += g;
  ctx->state[7] += h;
}


void Digest::SHA256Init(SHA256_CTX *ctx)
{
  // Initialize Hash values with
  // (first 32 bits of the fractional parts of the square roots of the first 8 primes 2..19):
  ctx->datalen = 0;
  ctx->bitlen[0] = 0;
  ctx->bitlen[1] = 0;
  ctx->state[0] = 0x6a09e667;
  ctx->state[1] = 0xbb67ae85;
  ctx->state[2] = 0x3c6ef372;
  ctx->state[3] = 0xa54ff53a;
  ctx->state[4] = 0x510e527f;
  ctx->state[5] = 0x9b05688c;
  ctx->state[6] = 0x1f83d9ab;
  ctx->state[7] = 0x5be0cd19;
}

void Digest::SHA256Update(SHA256_CTX *ctx, uchar data[], uint len)
{
  // Process the message in successive 512-bit chunks:
  for (uint i = 0; i < len; ++i) {
    ctx->data[ctx->datalen] = data[i];
    ctx->datalen++;
    if (ctx->datalen == 64) {
      Digest::SHA256Transform(ctx, ctx->data);
      DBL_INT_ADD(ctx->bitlen[0], ctx->bitlen[1], 512);
      ctx->datalen = 0;
    }
  }
}

void Digest::SHA256Final(SHA256_CTX *ctx, uchar hash[])
{
  uint i = ctx->datalen;

  // Post Processing:
  // append a single '1' bit
  // append K '0' bits, where K is the minimum number >= 0 such that L + 1 + K + 64 is a multiple of 512
  // append L as a 64-bit big-endian integer, making the total post-processed length a multiple of 512 bits
  if (ctx->datalen < 56) {
    ctx->data[i++] = 0x80;

    while (i < 56)
      ctx->data[i++] = 0x00;
  }
  else {
    ctx->data[i++] = 0x80;

    while (i < 64)
      ctx->data[i++] = 0x00;

    Digest::SHA256Transform(ctx, ctx->data);
    memset(ctx->data, 0, 56);
  }

  DBL_INT_ADD(ctx->bitlen[0], ctx->bitlen[1], ctx->datalen * 8);
  ctx->data[63] = ctx->bitlen[0];
  ctx->data[62] = ctx->bitlen[0] >> 8;
  ctx->data[61] = ctx->bitlen[0] >> 16;
  ctx->data[60] = ctx->bitlen[0] >> 24;
  ctx->data[59] = ctx->bitlen[1];
  ctx->data[58] = ctx->bitlen[1] >> 8;
  ctx->data[57] = ctx->bitlen[1] >> 16;
  ctx->data[56] = ctx->bitlen[1] >> 24;
  Digest::SHA256Transform(ctx, ctx->data);

  for (i = 0; i < 4; ++i) {
    hash[i] = (ctx->state[0] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 4] = (ctx->state[1] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 8] = (ctx->state[2] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 12] = (ctx->state[3] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 16] = (ctx->state[4] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 20] = (ctx->state[5] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 24] = (ctx->state[6] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 28] = (ctx->state[7] >> (24 - i * 8)) & 0x000000ff;
  }
}

