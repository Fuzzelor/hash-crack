/*
 *    hash-crack
 *    Copyright (C) 2018  Patrick Louis
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _H_DIGEST
#define _H_DIGEST 1

//#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

#define DEBUG_MODE 1

#define uchar unsigned char
#define uint unsigned int

#define DBL_INT_ADD(a,b,c) if (a > 0xffffffff - (c)) ++b; a += c;
#define ROTLEFT(a,b) (((a) << (b)) | ((a) >> (32-(b))))
#define ROTRIGHT(a,b) (((a) >> (b)) | ((a) << (32-(b))))

#define CH(x,y,z) (((x) & (y)) ^ (~(x) & (z)))
#define MAJ(x,y,z) (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))
#define EP0(x) (ROTRIGHT(x,2) ^ ROTRIGHT(x,13) ^ ROTRIGHT(x,22))
#define EP1(x) (ROTRIGHT(x,6) ^ ROTRIGHT(x,11) ^ ROTRIGHT(x,25))
#define SIG0(x) (ROTRIGHT(x,7) ^ ROTRIGHT(x,18) ^ ((x) >> 3))
#define SIG1(x) (ROTRIGHT(x,17) ^ ROTRIGHT(x,19) ^ ((x) >> 10))

#define MD5_F(x,y,z) (((x) & (y)) | ((~(x)) & (z)))
#define MD5_G(x,y,z) (((x) & (z)) | ((y) & ~(z)))
#define MD5_H(x,y,z) ((x) ^ (y) ^ (z))
#define MD5_I(x,y,z) ((y) ^ ((x) | ~(z)))
#define MD5_FF(a,b,c,d,x,s,ac) (a) = (ROTLEFT(((a) + MD5_F((b),(c),(d)) + (x) + (ac)),(s)) + (b))
#define MD5_GG(a,b,c,d,x,s,ac) (a) = (ROTLEFT(((a) + MD5_G((b),(c),(d)) + (x) + (ac)),(s)) + (b))
#define MD5_HH(a,b,c,d,x,s,ac) (a) = (ROTLEFT(((a) + MD5_H((b),(c),(d)) + (x) + (ac)),(s)) + (b))
#define MD5_II(a,b,c,d,x,s,ac) (a) = (ROTLEFT(((a) + MD5_I((b),(c),(d)) + (x) + (ac)),(s)) + (b))

namespace Digest {
  typedef struct {
	uchar data[64];
	uint datalen;
	uint bitlen[2];
	uint state[8];
  } SHA256_CTX;

  typedef struct {
	uchar data[64];
	uint datalen;
	uint bitlen[2];
	uint state[4];
  } MD5_CTX;

  std::string Sha_256(std::string input);
  void SHA256Transform(SHA256_CTX *ctx, uchar data[]);
  void SHA256Init(SHA256_CTX *ctx);
  void SHA256Update(SHA256_CTX *ctx, uchar data[], uint len);
  void SHA256Final(SHA256_CTX *ctx, uchar hash[]);


  std::string Md5(std::string data);
  std::string MD5PadData(std::string data, uint len);
  void MD5Init(MD5_CTX *ctx);
  void MD5Update(MD5_CTX *ctx, uchar data[], uint len);
  void MD5Transform(MD5_CTX *ctx, uchar data[]);
  void MD5Final(MD5_CTX *ctx, uchar hash[]);
  void encode(uchar output[], uint input[], uint len);
  void decode(uint output[], uchar input[], uint len);
}

#endif

