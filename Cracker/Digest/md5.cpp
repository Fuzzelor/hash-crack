/*
 *    hash-crack
 *    Copyright (C) 2018  Patrick Louis
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "digest.h"

namespace Digest {
	// Initialize Shift ammounts for each round
	#define S11 7
	#define S12 12
	#define S13 17
	#define S14 22
	#define S21 5
	#define S22 9
	#define S23 14
	#define S24 20
	#define S31 4
	#define S32 11
	#define S33 16
	#define S34 23
	#define S41 6
	#define S42 10
	#define S43 15
	#define S44 21

	std::string Md5(std::string data) {
		MD5_CTX ctx;
		std::string hash_string = "";
		unsigned char hash[16];

		data = MD5PadData(data, data.length());
		MD5Init(&ctx);
		MD5Update(&ctx, (uchar*)data.data(), (uint)data.size());
		MD5Final(&ctx, hash);


		char s[3];
		for (int i = 0; i < 16; i++) {
			sprintf(s, "%02x", hash[i]);
			hash_string += s;
		}

		return hash_string;
	}

	std::string MD5PadData(std::string data, uint len) {
		data.push_back(0x80);
		while ((data.size() * 8) % 512 != 448) {
			data.push_back(0x00);
		}
		unsigned long long origlen = (len * 8) % (UINT64_MAX);
		for (int i = 0; i <= 56; i += 8) {
			data.push_back((char)(origlen >> i));
		}
		return data;
	}

	void MD5Init(MD5_CTX *ctx) {
		ctx->datalen = 0;
		ctx->bitlen[0] = 0;
		ctx->bitlen[1] = 0;
		ctx->state[0] = 0x67452301; //A0
		ctx->state[1] = 0xefcdab89; //B0
		ctx->state[2] = 0x98badcfe; //C0
		ctx->state[3] = 0x10325476; //D0
	}

	void MD5Update(MD5_CTX *ctx, uchar data[], uint len) {
	  // Process the data in 512 Bit chunks
	  for (uint i = 0; i < len; ++i) {
		ctx->data[ctx->datalen] = data[i];
		ctx->datalen++;
		if (ctx->datalen == 64) {
		  MD5Transform(ctx, ctx->data);
		  DBL_INT_ADD(ctx->bitlen[0], ctx->bitlen[1], 512);
		  ctx->datalen = 0;
		}
	  }
	}


	void MD5Transform(MD5_CTX *ctx, uchar data[]) {
		uint a, b, c, d, m[16];

		// Split chunk into 16 32bit words
		for (int i = 0, j = 0; i < 16; i++, j+=4) {
			m[i] = (data[j]) | (data[j + 1] << 8) | (data[j + 2] << 16) | (data[j + 3] << 24);
		}

		// Initialize state vars
		a = ctx->state[0];
		b = ctx->state[1];
		c = ctx->state[2];
		d = ctx->state[3];

		/* Round 1 */
		MD5_FF (a, b, c, d, m[ 0], S11, 0xd76aa478); /* 1 */
		MD5_FF (d, a, b, c, m[ 1], S12, 0xe8c7b756); /* 2 */
		MD5_FF (c, d, a, b, m[ 2], S13, 0x242070db); /* 3 */
		MD5_FF (b, c, d, a, m[ 3], S14, 0xc1bdceee); /* 4 */
		MD5_FF (a, b, c, d, m[ 4], S11, 0xf57c0faf); /* 5 */
		MD5_FF (d, a, b, c, m[ 5], S12, 0x4787c62a); /* 6 */
		MD5_FF (c, d, a, b, m[ 6], S13, 0xa8304613); /* 7 */
		MD5_FF (b, c, d, a, m[ 7], S14, 0xfd469501); /* 8 */
		MD5_FF (a, b, c, d, m[ 8], S11, 0x698098d8); /* 9 */
		MD5_FF (d, a, b, c, m[ 9], S12, 0x8b44f7af); /* 10 */
		MD5_FF (c, d, a, b, m[10], S13, 0xffff5bb1); /* 11 */
		MD5_FF (b, c, d, a, m[11], S14, 0x895cd7be); /* 12 */
		MD5_FF (a, b, c, d, m[12], S11, 0x6b901122); /* 13 */
		MD5_FF (d, a, b, c, m[13], S12, 0xfd987193); /* 14 */
		MD5_FF (c, d, a, b, m[14], S13, 0xa679438e); /* 15 */
		MD5_FF (b, c, d, a, m[15], S14, 0x49b40821); /* 16 */

		/* Round 2 */
		MD5_GG (a, b, c, d, m[ 1], S21, 0xf61e2562); /* 17 */
		MD5_GG (d, a, b, c, m[ 6], S22, 0xc040b340); /* 18 */
		MD5_GG (c, d, a, b, m[11], S23, 0x265e5a51); /* 19 */
		MD5_GG (b, c, d, a, m[ 0], S24, 0xe9b6c7aa); /* 20 */
		MD5_GG (a, b, c, d, m[ 5], S21, 0xd62f105d); /* 21 */
		MD5_GG (d, a, b, c, m[10], S22, 0x02441453); /* 22 */
		MD5_GG (c, d, a, b, m[15], S23, 0xd8a1e681); /* 23 */
		MD5_GG (b, c, d, a, m[ 4], S24, 0xe7d3fbc8); /* 24 */
		MD5_GG (a, b, c, d, m[ 9], S21, 0x21e1cde6); /* 25 */
		MD5_GG (d, a, b, c, m[14], S22, 0xc33707d6); /* 26 */
		MD5_GG (c, d, a, b, m[ 3], S23, 0xf4d50d87); /* 27 */
		MD5_GG (b, c, d, a, m[ 8], S24, 0x455a14ed); /* 28 */
		MD5_GG (a, b, c, d, m[13], S21, 0xa9e3e905); /* 29 */
		MD5_GG (d, a, b, c, m[ 2], S22, 0xfcefa3f8); /* 30 */
		MD5_GG (c, d, a, b, m[ 7], S23, 0x676f02d9); /* 31 */
		MD5_GG (b, c, d, a, m[12], S24, 0x8d2a4c8a); /* 32 */

		/* Round 3 */
		MD5_HH (a, b, c, d, m[ 5], S31, 0xfffa3942); /* 33 */
		MD5_HH (d, a, b, c, m[ 8], S32, 0x8771f681); /* 34 */
		MD5_HH (c, d, a, b, m[11], S33, 0x6d9d6122); /* 35 */
		MD5_HH (b, c, d, a, m[14], S34, 0xfde5380c); /* 36 */
		MD5_HH (a, b, c, d, m[ 1], S31, 0xa4beea44); /* 37 */
		MD5_HH (d, a, b, c, m[ 4], S32, 0x4bdecfa9); /* 38 */
		MD5_HH (c, d, a, b, m[ 7], S33, 0xf6bb4b60); /* 39 */
		MD5_HH (b, c, d, a, m[10], S34, 0xbebfbc70); /* 40 */
		MD5_HH (a, b, c, d, m[13], S31, 0x289b7ec6); /* 41 */
		MD5_HH (d, a, b, c, m[ 0], S32, 0xeaa127fa); /* 42 */
		MD5_HH (c, d, a, b, m[ 3], S33, 0xd4ef3085); /* 43 */
		MD5_HH (b, c, d, a, m[ 6], S34, 0x04881d05); /* 44 */
		MD5_HH (a, b, c, d, m[ 9], S31, 0xd9d4d039); /* 45 */
		MD5_HH (d, a, b, c, m[12], S32, 0xe6db99e5); /* 46 */
		MD5_HH (c, d, a, b, m[15], S33, 0x1fa27cf8); /* 47 */
		MD5_HH (b, c, d, a, m[ 2], S34, 0xc4ac5665); /* 48 */

		/* Round 4 */
		MD5_II (a, b, c, d, m[ 0], S41, 0xf4292244); /* 49 */
		MD5_II (d, a, b, c, m[ 7], S42, 0x432aff97); /* 50 */
		MD5_II (c, d, a, b, m[14], S43, 0xab9423a7); /* 51 */
		MD5_II (b, c, d, a, m[ 5], S44, 0xfc93a039); /* 52 */
		MD5_II (a, b, c, d, m[12], S41, 0x655b59c3); /* 53 */
		MD5_II (d, a, b, c, m[ 3], S42, 0x8f0ccc92); /* 54 */
		MD5_II (c, d, a, b, m[10], S43, 0xffeff47d); /* 55 */
		MD5_II (b, c, d, a, m[ 1], S44, 0x85845dd1); /* 56 */
		MD5_II (a, b, c, d, m[ 8], S41, 0x6fa87e4f); /* 57 */
		MD5_II (d, a, b, c, m[15], S42, 0xfe2ce6e0); /* 58 */
		MD5_II (c, d, a, b, m[ 6], S43, 0xa3014314); /* 59 */
		MD5_II (b, c, d, a, m[13], S44, 0x4e0811a1); /* 60 */
		MD5_II (a, b, c, d, m[ 4], S41, 0xf7537e82); /* 61 */
		MD5_II (d, a, b, c, m[11], S42, 0xbd3af235); /* 62 */
		MD5_II (c, d, a, b, m[ 2], S43, 0x2ad7d2bb); /* 63 */
		MD5_II (b, c, d, a, m[ 9], S44, 0xeb86d391); /* 64 */

		ctx->state[0] += a;
		ctx->state[1] += b;
		ctx->state[2] += c;
		ctx->state[3] += d;
	}

	void MD5Final(MD5_CTX *ctx, uchar hash[]) {
		// Output is a b c d in small endian
		for (int i = 0; i < 4; ++i) {
			hash[i]		 = (ctx->state[0] >> (i * 8)) & 0x000000ff;
			hash[i + 4]  = (ctx->state[1] >> (i * 8)) & 0x000000ff;
			hash[i + 8]  = (ctx->state[2] >> (i * 8)) & 0x000000ff;
			hash[i + 12] = (ctx->state[3] >> (i * 8)) & 0x000000ff;
		}
	}
}
