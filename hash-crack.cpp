/*
 *   hash-crack
 *   Copyright (C) 2018  Patrick Louis
 *
 *    This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "hash-crack.h"

unsigned char toByte(char* character);
void print_help (void);

int main ( int argc, char** argv )
{
  //setvbuf(stdout,NULL,_IONBF,0);
  int option = 0;
  std::string target = "";

  Cracker* myCracker = new Cracker();

  // Read arguments
  while((option = getopt(argc, argv, "hd:a:c:t:v:")) != -1 ){
    switch (option) {
      case 'h':
        print_help();
        return 0;
      case 'a':
        myCracker->setAlgorithm(optarg);
        break;
      case 'c':
        myCracker->setCharSet(toByte(optarg));
        break;
      case 'v':
        target.assign(optarg);
        break;
      case 't':
        myCracker->setThreadCount(toByte(optarg));
        break;
      case '?':
        if (optopt == 'c' || optopt == 'a' || optopt == 't' || optopt == 'T')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
        return 1;
      default:
        abort();
    }
  }

  // Check if target was given
  if (target == "") {
    fprintf(stderr,"Target must be given to operate on\n");
    return -1;
  }

  printf("Starting... \n");
  // Crack the password
  std::string result = myCracker->Crack(target);
  printf("----------------------------------------------------\n" );
  printf("Result found '%s' --[%s]--> %s\n", result.data(), myCracker->getAlgorithm().data(), target.data());
  printf("----------------------------------------------------\n" );
  return 0;
}

unsigned char toByte(char* character) {
  int i = 0;
  unsigned char result = 0;
  while ( character[i] != '\x00' ) {
    result *= 10;
    result += character[i] - '0';
    i++;
  }
  return result;
}


void print_help (void) {
  printf("%s Version %d.%d\n\n",APPLICATION_NAME,VERSION_MAJOR,VERSION_MINOR);
  printf("%s %s %s %s %s\n", APPLICATION_NAME,"[-h]","[-a <algorithm>] [-t <threadCount>]","[-c <charset>]","-v <targetHash>");
  printf("---------------------------------------------------------------\n");
  printf("%s\t\t\t%s\n","-h","Shows this text");
  printf("%s %s\t\t%s\n","-a","<algorithm>","Set the hashing algorithm to use; supports sha256 and md5 at the moment");
  printf("%s %s\t\t%s\n","-c","<charset>","Set the characterset to use for bruteforcing; This value is a bitmap with the following mapping:");
  printf("\t\t\t%s\n","00000001 - Lowercase Alpha");
  printf("\t\t\t%s\n","00000010 - Uppercase Alpha");
  printf("\t\t\t%s\n","00000100 - Numeric Characters");
  printf("\t\t\t%s\n","00001000 - Special Characters");
  printf("%s %s\t%s\n","-t","<threadCount>","The ammount of threads to use");
  printf("%s %s\t\t%s\n","-v","<targetHash>","The hash to bruteforce");
}
